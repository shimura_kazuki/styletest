<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>login</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/test.css" />" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="errorMessages">
			<c:if test="${ not empty errorMessages }">
				  <ul class="alert alert-danger">
				      <c:forEach items="${errorMessages}" var="message">
				          <li><c:out value="${message}" />
				      </c:forEach>
				  </ul>
			<c:remove var="errorMessages" scope="session" />
			</c:if>
		</div>
		<h1>ログイン</h1>
			<c:out value = "${message}"></c:out>
		<form:form modelAttribute="userForm">
		<div class="form-group form-row">
			<label for="employeeId" class="col-md-2">社員番号</label>
			<form:input path = "employeeId" type="number" class="form-control col-md-6" id="employeeId"/>
		</div>
		<div class="form-group form-row">
			<label for="Password" class="col-md-2">パスワード</label>
			<form:input path = "password" class="form-control col-md-6" id="Password" type="password"/>
		</div>
		<div class="form-group form-row">
			<button type="submit" class="btn btn-primary">ログイン</button>
		</div>
		</form:form>
	</div>
	<div class="copyright">Copyright (c) Hoshi , Ushigami , Shimura</div>
</body>
</html>