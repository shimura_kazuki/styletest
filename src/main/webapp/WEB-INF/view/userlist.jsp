<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザー一覧</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/test.css" />" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
	<script type="text/javascript">
		function stopCheck(id) {
			console.log("停止"+id)
			var result = window.confirm("停止しますか?")
			if(result){
				window.location.href="${pageContext.request.contextPath}/user/isstop/"+ id +"/";
			} else {
				return false;
			}
		}

		function revivalCheck(id) {
			console.log("復活"+id)
			var result = window.confirm("復活しますか?")
			if(result){
				window.location.href="${pageContext.request.contextPath}/user/isstart/"+ id +"/";
			} else {
				return false;
			}
		}

	</script>

<body>
	<nav class="navbar navbar-expand navbar-dark bg-dark">
		<div class="container">
			<div class="collapse navbar-collapse">
				<div class="navbar-nav">
					<c:if test = "${ loginUser.adminFlag == 1}">
						<a class="nav-item nav-link" href="${pageContext.request.contextPath}/signup/">新規ユーザー登録</a>
					</c:if>
						<a class="nav-item nav-link" href="${pageContext.request.contextPath}/index/">トップへ</a>
					<c:if test = "${ loginUser.adminFlag == 1}">
						<a class="nav-item nav-link" href="${pageContext.request.contextPath}/summary/">サマリー表示</a>
						<a class="nav-item nav-link" href="${pageContext.request.contextPath}/user/answered/"
						onclick = "return confirm('解答権をリセットします。よろしいですか？')">リセット</a>
					</c:if>
				</div>
			</div>
		</div>
	</nav>
	<c:out value = "${message}"></c:out>
	<div class="container">
	 	<h4>絞り込み</h4>
	 	<div class="form-group form-row">
			<select name="change_js" class="form-control  col-md-4">
				<c:forEach items="${ units }" var="unit">
					<option value="${pageContext.request.contextPath}/user/list/${unit.id}/">${ unit.name }</option>
				</c:forEach>
			</select>
			<input type="button"  value="ユーザー一覧(全ユニット)"
					onclick="location.href='${pageContext.request.contextPath}/user/list/'" class="btn btn-primary ml-3">
		</div>
		<form name = "StyleTest">
			<h3>ユーザー一覧</h3>
			<c:forEach items = "${userList}" var = "user" >
				<p>名前：<c:out value = "${user.name}"></c:out></p>
				<p>所属Unit：
					<c:forEach items="${ units }" var="unit">
						<c:if test="${ user.unitId == unit.id }">
							<c:out value="${unit.name}"></c:out>
						</c:if>
					</c:forEach>
				</p>
				<input type="button"  value="ユーザー詳細表示"
					onclick="location.href='${pageContext.request.contextPath}/index/${user.id}/'" class="btn btn-primary ml-3">

				<c:if test = "${ loginUser.adminFlag == 1}">
					<input type="button"  value="ユーザー編集"
						onclick="location.href='${pageContext.request.contextPath}/user/update/${user.id}/'" class="btn btn-primary ml-3" >
					<c:if test="${user.isStopped == 1 }">
	    				<input type="button"  value="復活" name ="${user.id}"  onclick= "revivalCheck(this.name)" class="btn btn-primary ml-3">
	    			</c:if>
					<c:if test="${user.isStopped == 0 }">
						<input type="button"  value="停止" name = "${user.id}" onclick="stopCheck(this.name)" class="btn btn-danger ml-3">
	    			</c:if>
					<c:if test="${user.isAnswered == 1 }">
						<input type="button"  value="回答権復活"
						onclick="location.href='${pageContext.request.contextPath}/user/isanswered/${user.id}/'"  class="btn btn-primary ml-3">
	    			</c:if>
	    		</c:if>


	    		<hr>
			</c:forEach>
		</form>
	</div>
	<div class="copyright">Copyright (c) Hoshi , Ushigami , Shimura</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
	const selected = $("select[name=change_js]");
	selected.on('change', function(){
	window.location.href = selected.val();
	});
</script>

</body>
</html>