<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>styleTest</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/test.css" />" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand navbar-dark bg-dark" id="mainNav">
		<div class="container">
			<div class="collapse navbar-collapse">
				<div class="navbar-nav">
				<c:choose>
					<c:when test = "${TestId  == null}">
						<<a class="nav-item nav-link" href="${pageContext.request.contextPath}/index/">トップへ</a>
					</c:when>
					<c:otherwise>
						<<a class="nav-item nav-link" href="${pageContext.request.contextPath}/index/">トップへ</a>
					</c:otherwise>
				</c:choose>

				</div>
			</div>
		</div>
	</nav>
	<div class="container">
		<h1>StyleTest</h1>
		<form:form modelAttribut="StyleTestResultForm"  name="StyleTests" id="form">
			<c:forEach items="${ styleTests }" var="TestList" varStatus="status" >
				<div class="questionBox">
					<span class="questionTitle"><c:out value = "${TestList.question}"></c:out></span>
					<p><c:out value = "A：${TestList.driveType}"></c:out>
					<p><c:out value = "B：${TestList.analyzeType}"></c:out>
					<p><c:out value = "C：${TestList.createType}"></c:out>
					<p><c:out value = "D：${TestList.volunteerType}"></c:out>
					<div class="container">
						<span class="control-label ">非常によく当てはまる</span>
							<label class="btn btn-outline-primary">
								<span>A</span>
								<input id="D2" type="radio" name="applyweel${status.index}"
									class="applyweel${status.index}_d2 validate[required]" value="2" required>
							</label>
							<label class="btn btn-outline-primary">
								<span>B</span>
								<input id="A2" type="radio" name="applyweel${status.index}"
									class="applyweel${status.index}_a2 validate[required]" value="2" required>
							</label>
							<label class="btn btn-outline-primary">
								<span>C</span>
								<input id="C2" type="radio" name="applyweel${status.index}"
									class="applyweel${status.index}_c2 validate[required]" value="2" required>
							</label>
							<label class="btn btn-outline-primary">
								<span>D</span>
								<input id="V2" type="radio" name="applyweel${status.index}"
									class="applyweel${status.index}_v2 validate[required]" value="2" required>
							</label>
						<span class="control-label ml-4">当てはまる</span>
							<label class="btn btn-outline-info">
								<span>A</span>
								<input id="D1" type="radio" name="apply${status.index}"
									class="apply${status.index}_d1" value="1" required>
							</label>
							<label  class="btn btn-outline-info">
								<span>B</span>
								<input id="A1" type="radio" name="apply${status.index}"
									class="apply${status.index}_a1" value="1" required>
							</label>
							<label  class="btn btn-outline-info">
								<span>C</span>
								<input id="C1" type="radio" name="apply${status.index}"
									class="apply${status.index}_c1" value="1"  required>
							</label>
							<label  class="btn btn-outline-info">
								<span>D</span>
								<input id="V1" type="radio" name="apply${status.index}"
									class="apply${status.index}_v1" value="1"  required>
							</label>
					</div>
				</div>
			</c:forEach>

			<input type = "hidden" name = "userId" id = "userId" value = "${id} ">
			<input type = "hidden" name = "DrivePoint" id = "DrivePoint" value = "0">
			<input type = "hidden" name = "AnalyzePoint" id = "AnalyzePoint" value = "0">
			<input type = "hidden" name = "CreatePoint" id = "CreatePoint" value = "0">
			<input type = "hidden" name = "VolunteerPoint" id = "VolunteerPoint" value = "0">

			<div class="form-group form-row">
				<button type="submit" id = "button" class="btn btn-info ml-5">回答を終了する</button>
			</div>
		</form:form>
	</div>
	<div class="copyright">Copyright (c) Hoshi , Ushigami , Shimura</div>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<c:forEach items = "${ styleTests}" var = "TestList" varStatus = "status" >
<script>
$("#button").on("click", function(){
	var DP = 0;
	var DrivePoint =  parseInt(DP);
	var AP = 0;
	var AnalyzePoint =  parseInt(AP);
	var CP = 0;
	var CreatePoint =  parseInt(CP);
	var VP = 0;
	var VolunteerPoint =  parseInt(VP);

	$('input#D2:checked').each(function(){
		var D2point = $(this).val();
		var D2 = parseInt(D2point);
		DrivePoint = DrivePoint + D2;
	})
		$('input#D1:checked').each(function(){
		var D1point = $(this).val();
		var D1 = parseInt(D1point);
		DrivePoint = DrivePoint + D1;
	})
		$('input#A2:checked').each(function(){
		var A2point = $(this).val();
		var A2 = parseInt(A2point);
		AnalyzePoint = AnalyzePoint + A2;
	})
		$('input#A1:checked').each(function(){
		var A1point = $(this).val();
		var A1 = parseInt(A1point);
		AnalyzePoint = AnalyzePoint + A1;
	})
		$('input#C2:checked').each(function(){
		var C2point = $(this).val();
		var C2 = parseInt(C2point);
		CreatePoint = CreatePoint + C2;
	})
		$('input#C1:checked').each(function(){
		var C1point = $(this).val();
		var C1 = parseInt(C1point);
		CreatePoint = CreatePoint + C1;
	})
		$('input#V2:checked').each(function(){
		var V2point = $(this).val();
		var V2 = parseInt(V2point);
		VolunteerPoint = VolunteerPoint + V2;
	})
		$('input#V1:checked').each(function(){
		var V1point = $(this).val();
		var V1 = parseInt(V1point);
		VolunteerPoint = VolunteerPoint + V1;
	})

	$('#DrivePoint').val(DrivePoint);
	$('#AnalyzePoint').val(AnalyzePoint);
	$('#CreatePoint').val(CreatePoint);
	$('#VolunteerPoint').val(VolunteerPoint);
})
</script>
<script>
	$('.applyweel' + ${status.index} + '_d2').change(function() {
	    var result = $('.applyweel' + ${status.index} + '_d2').prop('checked');			//押したボタンのchecked属性の状態を取得する
	    if ( result ) { $('.apply' + ${status.index} + '_d1').prop('checked', false); }		//押したボタンがチェックされていればchecked属性を付与する
	    console.log("${status.index}-d2");
	})
	$('.apply' + ${status.index} + '_d1').change(function() {
	    var result = $('.apply' + ${status.index} + '_d1').prop('checked');
	    if ( result ) { $('.applyweel' + ${status.index} + '_d2').prop('checked', false); }
	    console.log("${status.index}-d1");
	})
	$('.applyweel' + ${status.index} + '_a2').change(function() {
	    var result = $('.applyweel' + ${status.index} + '_a2').prop('checked');
	    if ( result ) { $('.apply' + ${status.index} + '_a1').prop('checked', false); }
	    console.log("${status.index}-a2");
	})
	$('.apply' + ${status.index} + '_a1').change(function() {
	    var result = $('.apply' + ${status.index} + '_a1').prop('checked');
	    if ( result ) { $('.applyweel' + ${status.index} + '_a2').prop('checked', false); }
	    console.log("${status.index}-a1");
	})
	$('.applyweel' + ${status.index} + '_c2').change(function() {
	    var result = $('.applyweel' + ${status.index} + '_c2').prop('checked');
	    if ( result ) { $('.apply' + ${status.index} + '_c1').prop('checked', false); }
	    console.log("${status.index}-c2");
	})
	$('.apply' + ${status.index} + '_c1').change(function() {
	    var result = $('.apply' + ${status.index} + '_c1').prop('checked');
	    if ( result ) { $('.applyweel' + ${status.index} + '_c2').prop('checked', false); }
	    console.log("${status.index}-c1");
	})
	$('.applyweel' + ${status.index} + '_v2').change(function() {
	    var result = $('.applyweel' + ${status.index} + '_v2').prop('checked');
	    if ( result ) { $('.apply' + ${status.index} + '_v1').prop('checked', false); }
	    console.log("${status.index}-v2");
	})
	$('.apply' + ${status.index} + '_v1').change(function() {
	    var result = $('.apply' + ${status.index} + '_v1').prop('checked');
	    if ( result ) { $('.applyweel' + ${status.index} + '_v2').prop('checked', false); }
	    console.log("${status.index}-v1");
	})
</script>
<script>
$('#form').validate({
    // バリデーションルール
    rules: {
    },
    // エラーメッセージ
    messages: {
    	applyweel${status.index}: {
            required: '非常によく当てはまるを選択してください',
        },
    },
    // エラーメッセージ出力箇所
    errorPlacement: function(error, element){
        var name = element.attr('name');
        if (element.attr('name') === name){
            error.appendTo($('.is-error-'+name));
        }
    },
    errorElement: "span",
    errorClass: "is-error",
});

$('#form').validate({
    // バリデーションルール
    rules: {
    },
    // エラーメッセージ
    messages: {
    	apply${status.index} {
            required: 'よく当てはまるを選択してください',
        },
    },
    // エラーメッセージ出力箇所
    errorPlacement: function(error, element){
        var name = element.attr('name');
        if (element.attr('name') === name){
            error.appendTo($('.is-error-'+name));
        }
    },
    errorElement: "span",
    errorClass: "is-error",
});
</script>
<style>
input[type=radio] {
    width: 25px;
    height: 25px;
    vertical-align: middle;
}
</style>
</c:forEach>


</body>
</html>