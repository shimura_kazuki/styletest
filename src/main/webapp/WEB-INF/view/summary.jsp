<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>サマリー</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/test.css" />" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<body>
	<nav class="navbar navbar-expand navbar-dark bg-dark">
		<div class="container">
			<div class="collapse navbar-collapse">
				<div class="navbar-nav">
					<a class="nav-item nav-link" href="${pageContext.request.contextPath}/user/list/">ユーザー一覧</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="container">
	<h1>ALH全体での割合</h1>
		<canvas id="myChart"></canvas>
	</div>
	<div class="container">
		<div class="driveBox">
			<span class="driveTitle">drive(<c:out value="${drivePoint}"></c:out>%)</span>
				<p>欲求：達成支配欲求</p>
				<p>KEY：勝ち負け、敵味方、損得</p>
				<p>初対面：なんぼのもんだ</p>
				<p>HappyWord：すごい</p>
				<p>ストレス：結果の数値化があいまい、競争の原理があいまい</p>
				<p>強み：牽引力、主張性、主体性、達成意欲</p>
				<p>課題：受容力、傾聴力、中立性、自律性</p>
				<p>趣向：自力本願で強くありたい、周囲に影響を与えたい、意志薄弱状態や他者依存を避けたい
								  プロセスより結果、目的や目標達成を追求、他者との競争を勝ち抜く</p>
		</div>

		<div class="analyzeBox">
			<span class="analyzeTitle">analyze(<c:out value="${analyzePoint}"></c:out>%)</span>
				<p>欲求：論理探求欲求</p>
				<p>KEY：真偽、因果、優劣</p>
				<p>初対面：言っていることは正しいのか</p>
				<p>HappyWord：確かに、正しい</p>
				<p>ストレス：大雑把な物事、合理的な意味付けのない仕事</p>
				<p>強み：分析力、計画力、探求心、規律性</p>
				<p>課題：発想力、直感力、柔軟性、変化志向</p>
				<p>趣向：様々な知識を吸収したい、複雑な物事を究明して自信を持ちたい勢いだけで走ることを避けたい、
								無計画な状態を避けたい、合理的な答えを導き出したい、客観的な視点を軽視しない</p>
		</div>
		<div class="createBox">
		<span class="createTitle">create(<c:out value="${createPoint}"></c:out>%)</span>
				<p>欲求：感性発散欲求</p>
				<p>KEY：好き嫌い、苦楽、美醜</p>
				<p>初対面：面白い人なのか</p>
				<p>HappyWord：新しい、面白い</p>
				<p>ストレス：マニュアル化の域を出ない、アイディア発揮の余地がない</p>
				<p>強み：発想力、直観力、柔軟性、変化志向</p>
				<p>課題：持続力、計画力、探求心、規律性</p>
				<p>趣向：新しいものを生み出したい、楽しいことを計画したい、自分の個性を理解されたい、平凡であることを避けたい
								同じことの繰り返しを避けたい、変化感や変革を実現したい、自由な発想・型にはまらないことを望む</p>
		</div>
		<div class="volunteerBox">
			<span class="volunteerTitle">volunteer(<c:out value="${volunteerPoint}"></c:out>%)</span>
				<p>欲求：貢献調停欲求</p>
				<p>KEY：善悪、正邪、愛憎</p>
				<p>初対面：いい人なのかどうか</p>
				<p>HappyWord：ありがとう</p>
				<p>ストレス：激しい競争</p>
				<p>強み：調停力、受容力、中立性、自制心</p>
				<p>課題：牽引力、主張力、主体性、達成意欲</p>
				<p>趣向：人から愛されたい、平和を保ち葛藤を避けたい、中立的な立場にいたい、他者との戦いより協調を大切にしたい
								  周囲の期待に応えたい、人からの支持・感謝されたい、結果よりプロセス</p>
		</div>
	</div>
	<div class="copyright">Copyright (c) Hoshi , Ushigami , Shimura</div>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
	type: 'doughnut',
	data: {
		labels: ['drive', 'volunteer', 'analyze', 'create'],
			datasets: [{
				label: 'label',

				//ここにJavaから渡された値を入れればよいはず
				data: [<c:out value="${drivePoint}"></c:out>, <c:out value="${analyzePoint}"></c:out>,
				 		<c:out value="${createPoint}"></c:out>, <c:out value="${volunteerPoint}"></c:out>],
				backgroundColor: ["rgba(255,0,0,0.8)", "rgba(0,255,0,0.8)","rgba(0,0,255,0.8)","rgba(255,242,0,0.8)"]
		}]
	}
});
</script>
</body>
</html>
