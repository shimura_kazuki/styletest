<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>ホーム</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/test.css" />" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>
<body>
	<nav class="navbar navbar-expand navbar-dark bg-dark">
		<div class="container">
			<div class="collapse navbar-collapse">
				<div class="navbar-nav">
					<a class="nav-item nav-link" href="${pageContext.request.contextPath}/user/list/">ユーザー一覧</a>
					<c:if test="${loginUser.isAnswered == 0 }">
						<a class="nav-item nav-link" href="${pageContext.request.contextPath}/styleTest/">スタイルテスト</a>
					</c:if>
					<a class="nav-item nav-link" href="${pageContext.request.contextPath}/logout/">ログアウト</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
			  <ul>
			      <c:forEach items="${errorMessages}" var="message">
			          <li><c:out value="${message}" />
			      </c:forEach>
			  </ul>
		<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>
	<div class="container">
		<h1><c:out value="${name}" />さんの割合</h1>

		<c:if test = "${ tryCount> 1}">
			<c:out value = "${testDate}"/>実施分<br>
		</c:if>

		<c:if test = "${ tryCount == 0}">
			テスト未実施<br>
		</c:if>
			<c:out value = "${message}"></c:out>

			<canvas id="myChart"></canvas>

		<c:forEach items = "${otherResults}" var = "otherResults"  >
			<a class="nav-item nav-link" href="${pageContext.request.contextPath}/${otherResults.id}/">${otherResults.createdDate}</a>
		</c:forEach>
		<div class="copyright">Copyright (c) Hoshi , Ushigami , Shimura</div>
	</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
	type: 'doughnut',
	data: {
		labels: ['drive', 'volunteer', 'analyze', 'create'],
			datasets: [{
				label: 'label',


				//ここにJavaから渡された値を入れればよいはず
				data: [<c:out value="${drivePoint}"></c:out>, <c:out value="${analyzePoint}"></c:out>,
				 		<c:out value="${createPoint}"></c:out>, <c:out value="${volunteerPoint}"></c:out>],
				backgroundColor: ["rgba(255,0,0,0.8)", "rgba(0,255,0,0.8)","rgba(0,0,255,0.8)","rgba(255,242,0,0.8)"]
		}]
	}
});
</script>
</body>
</html>
