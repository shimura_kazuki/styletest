<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>新規ユーザー登録</title>
	<link rel="stylesheet" href="<c:url value="/resources/css/test.css" />" >
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand navbar-dark bg-dark" id="mainNav">
		<div class="container">
			<div class="collapse navbar-collapse">
				<div class="navbar-nav">
					<a class="nav-item nav-link" href="${pageContext.request.contextPath}/user/list/">ユーザー一覧</a>
				</div>
			</div>
		</div>
	</nav>
	<div class="errorMessages">
		<c:if test="${ not empty errorMessages }">
			  <ul  class="alert alert-danger">
			      <c:forEach items="${errorMessages}" var="message">
			          <li><c:out value="${message}" />
			      </c:forEach>
			  </ul>
		<c:remove var="errorMessages" scope="session" />
		</c:if>
	</div>
	<div class="container">
		<h1>新規ユーザー登録</h1>
		<h2>新規登録するユーザー情報を入力してください</h2>
		<form:form modelAttribute="userForm">
		<div class="form-group form-row">
			<label for="employeeId" class="col-md-2">社員番号</label>
			<form:input path="employeeId" type="number" class="form-control col-md-6" id="employeeId"/>
		</div>
		<div class="form-group form-row">
			<label for="name" class="col-md-2">名前</label>
			<form:input path = "name" class="form-control col-md-6" id="name"/>
		</div>
		<div class="form-group form-row">
			<label for="Password" class="col-md-2">パスワード</label>
			<form:input path = "password" type="password" class="form-control col-md-6" id="Password"/>
		</div>
		<div class="form-group form-row">
			<label for="confirmPassword" class="col-md-2">確認用パスワード</label>
			<form:input path = "confirmPassword" type="password" class="form-control col-md-6" id="confirmPassword"/>
		</div>
		<div class="form-group form-row">
			<label for="unitId" class="col-md-2">所属Unit</label>
			<form:select path="unitId" class="form-control col-md-6" id="unitId">
				<c:forEach items="${ units }" var="unit">
					<form:option  value="${ unit.id }">${ unit.name }</form:option>
				</c:forEach>
			</form:select>
			</div>
			<div class="form-group form-row">
				<label for="unitId" class="col-md-2">管理者権限</label>
					あり<input type="radio" name="adminFlag" value="1">
					なし<input type="radio" name="adminFlag" value="0" checked = "checked">
			</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary">登録</button>
		</div>
		</form:form>
	</div>
	<div class="copyright">Copyright (c) Hoshi , Ushigami , Shimura</div>
</body>
</html>
