package styletest.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import styletest.dto.UnitDto;
import styletest.entity.Unit;
import styletest.mapper.UnitMapper;

@Service
public class UnitService {

	@Autowired
	private UnitMapper unitMapper;

		//取ってきたUnit情報(units)を(unitList)に詰めるためのメソッドを作る作業
		public List<UnitDto> getUnits() {
			List <Unit> unitList = unitMapper.getUnits();
			List<UnitDto> resultList = convertToDto(unitList);
			return resultList;
		}

		private List<UnitDto> convertToDto(List<Unit> unitList) {
			List<UnitDto> resultList = new LinkedList<>();
			for (Unit entity : unitList) {
				UnitDto dto = new UnitDto();
				BeanUtils.copyProperties(entity, dto);
				resultList.add(dto);
			}
			return resultList;
		}


}
