package styletest.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import styletest.dto.OtherTestResultDto;
import styletest.dto.StyleTestDto;
import styletest.dto.StyleTestResultDto;
import styletest.entity.OtherTestResult;
import styletest.entity.StyleTest;
import styletest.entity.StyleTestResult;
import styletest.mapper.StyleTestMapper;

@Service
public class StyleTestService {

	@Autowired
	private StyleTestMapper styleTestMapper;

		//	問題文取得(GETメソッド)
		public ArrayList<StyleTestDto> getStyleTest() {
			ArrayList<StyleTest> testList = new ArrayList<StyleTest>();
			testList = styleTestMapper.getTest();
			ArrayList<StyleTestDto> resultTestList = new ArrayList<StyleTestDto>();
			resultTestList = convertToDto(testList);
			return resultTestList;
	    }

		//entity⇔Dtoコンバート
		private ArrayList<StyleTestDto> convertToDto(ArrayList<StyleTest> testList) {
			ArrayList<StyleTestDto> resultList = new ArrayList<StyleTestDto>();
			for(StyleTest entity : testList) {
				StyleTestDto dto = new StyleTestDto();
				BeanUtils.copyProperties(entity, dto);
				resultList.add(dto);
			}
			return resultList;
		}

		//	診断結果格納
		public void ResultResister(StyleTestResultDto dto) {
			StyleTestResult entity = new StyleTestResult();
			BeanUtils.copyProperties(dto, entity);
			styleTestMapper.resister(entity);

		}

		//スタイルテスト結果取得(1件)
		public StyleTestResultDto getTestResult(int id) {
			StyleTestResult entity = styleTestMapper.getStyleTestResult(id);
			StyleTestResultDto dto = new StyleTestResultDto();
			BeanUtils.copyProperties(entity, dto);
			return dto;
		}

		//	過去のテスト日時等取得
		public ArrayList<StyleTestResultDto> getOtherResult(int id) {
			ArrayList<StyleTestResult> entity = styleTestMapper.getOtherStyleTestResult(id);
			ArrayList<StyleTestResultDto> otherResults = new ArrayList<StyleTestResultDto>();
			otherResults = OtherResultConvertToDto(entity);
			return otherResults;
		}


		private ArrayList<StyleTestResultDto> OtherResultConvertToDto(ArrayList<StyleTestResult> otherTestResultList) {
		ArrayList<StyleTestResultDto> trl = new ArrayList<StyleTestResultDto>();
		for ( StyleTestResult entity : otherTestResultList ) {
			StyleTestResultDto dto = new StyleTestResultDto();
			BeanUtils.copyProperties(entity, dto);
			trl.add(dto);
		}
			return trl;
		}


	public List<StyleTestResultDto> getTestResultList() {
		List<StyleTestResult> testResultList = styleTestMapper.getTestResultList();
		List<StyleTestResultDto> trl = convertToDto(testResultList);
		return trl ;
	}

    private List<StyleTestResultDto> convertToDto(List<StyleTestResult> testResultList) {
        List<StyleTestResultDto> trl = new LinkedList<>();
        for (StyleTestResult entity : testResultList) {
        	StyleTestResultDto dto = new StyleTestResultDto();
            BeanUtils.copyProperties(entity, dto);
            trl.add(dto);
        }
        return trl;
    }

    public OtherTestResultDto getOtherTestResult(int id) {
    	OtherTestResult otherTestresult = styleTestMapper.getOtherTestResult(id);
    	OtherTestResultDto otherTestResultDto = new OtherTestResultDto();
    	
    	System.out.println("otherTestresult"+otherTestresult);
    	BeanUtils.copyProperties(otherTestresult, otherTestResultDto);

    	return otherTestResultDto;
    }

    public void setTestResultFlag(int userId) {
    	styleTestMapper.setTestResultFlag(userId);
    }

    public List<StyleTestResultDto> getTryCount(int userId) {
    	List<StyleTestResult> tryCount =  styleTestMapper.getTryCount(userId);
    	List<StyleTestResultDto> tryCountDto = convertToDto(tryCount);

    	return tryCountDto;

    }

	public ArrayList<StyleTestResultDto> userStyleTestCheck(int id) {
		ArrayList<StyleTestResult> entity = styleTestMapper.userStyleTestCheck(id);
		ArrayList<StyleTestResultDto> ExistenceUsers = convertToDto2(entity);
		return ExistenceUsers;
	}

    private ArrayList<StyleTestResultDto> convertToDto2(ArrayList<StyleTestResult> userList){
    	ArrayList<StyleTestResultDto> resultList = new ArrayList<StyleTestResultDto>();
    	for(StyleTestResult entity : userList) {
    		StyleTestResultDto dto = new StyleTestResultDto();
    		BeanUtils.copyProperties(entity, dto);
    		resultList.add(dto);
    	}
    	return resultList;
    }
}
