package styletest.service;

import java.util.ArrayList;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import styletest.dto.UserDto;
import styletest.entity.User;
import styletest.mapper.UserMapper;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

		 //ユーザー新規登録
	    public int insertUser(UserDto dto) {
	    	int count = userMapper.insertUser(dto);
			return count;
		}

		//ユーザー一覧取得
	    public ArrayList<UserDto> getUserList() {
	    	ArrayList<User> userList = new ArrayList<User>();
	    	userList = userMapper.getUserList();
	    	ArrayList<UserDto> resultUserList = new ArrayList<UserDto>();
	    	resultUserList = convertToDto2(userList);
	    	return resultUserList;
	    }
	  //ユーザー一覧取得(検索あり)
	    public ArrayList<UserDto> searchUserList(int unitId) {
	    	ArrayList<User> userList = new ArrayList<User>();
	    	userList = userMapper.searchUserList(unitId);
	    	ArrayList<UserDto> resultUserList = new ArrayList<UserDto>();
	    	resultUserList = convertToDto2(userList);
	    	return resultUserList;
	    }

	    private ArrayList<UserDto> convertToDto2(ArrayList<User> userList){
	    	ArrayList<UserDto> resultList = new ArrayList<UserDto>();
	    	for(User entity : userList) {
	    		UserDto dto = new UserDto();
	    		BeanUtils.copyProperties(entity, dto);
	    		resultList.add(dto);
	    	}
	    	return resultList;
	    }

	    //ユーザー情報更新
	  	public int updateUser(UserDto dto) {
	  		int count = userMapper.updateUser(dto);
	  		return count;
	  	}

	    //ユーザー情報を取得
		public UserDto getUser(int id) {
			UserDto dto = new UserDto();
			User entity = userMapper.getUser(id);
			BeanUtils.copyProperties(entity, dto);
			return dto;
		}

	    //ユーザー情報を取得
		public UserDto getUser2(int EmployeeId) {
			UserDto dto = new UserDto();
			User entity = userMapper.getUser2(EmployeeId);

			if(entity == null) {
				return null;
			} else {
				BeanUtils.copyProperties(entity, dto);
				return dto;
			}
		}

	    public UserDto login(UserDto dto) {
	    	User entity = new User();
	    	BeanUtils.copyProperties(dto, entity);
	    	ArrayList<User> loginUsers = new ArrayList<User>();
	    	loginUsers = userMapper.login(entity);


	    	if ( loginUsers.size() == 0 ) {
	    		return null;
	    	} else if ( 2 <= loginUsers.size() ) {
	    		return null;
	    	} else {
	    		UserDto convertUser = new UserDto();
	    		convertUser = convertToDto(loginUsers.get(0));
	    		return convertUser;
	    	}
	    }

	    private UserDto convertToDto(User loginUsers){
    		UserDto dto = new UserDto();
    		User entity = loginUsers;
    		BeanUtils.copyProperties(entity, dto);
	    	return dto;
	    }

    //ユーザー復活停止
  	public int isStop(UserDto dto) {
  		int count = userMapper.isStop(dto);
  		return count;
  	}
	public int isAnswer(UserDto dto) {
		int count = userMapper.isAnswer(dto);
  		return count;
	}
	public int isAnswerAll(int IsAnswered) {
		int count = userMapper.isAnswerAll(IsAnswered);
  		return count;
	}

	public ArrayList<UserDto> userCheck(int id){
		ArrayList<User> entity = userMapper.userCheck(id);
		ArrayList<UserDto> ExistenceUsers = convertToDto2(entity);
		return ExistenceUsers;
	}
}
