package styletest.entity;

import java.sql.Date;

public class StyleTest {

	private int id;
	private String question;
	private String driveType;
	private String analyzeType;
	private String createType;
	private String volunteerType;
	private Date createdDate;
	private Date updatedDate;


	public int getId() {
		return id;
	}

	public String getQuestion() {
		return question;
	}

	public String getDriveType() {
		return driveType;
	}

	public String getAnalyzeType() {
		return analyzeType;
	}

	public String getCreateType() {
		return createType;
	}

	public String getVolunteerType() {
		return volunteerType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public void setDriveType(String driveType) {
		this.driveType = driveType;
	}

	public void setAnalyzeType(String analyzeType) {
		this.analyzeType = analyzeType;
	}

	public void setCreateType(String createType) {
		this.createType = createType;
	}

	public void setVolunteerType(String volunteerType) {
		this.volunteerType = volunteerType;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
