package styletest.entity;

import java.sql.Date;

public class StyleTestResult {

	private int id;
	private int userId;
	private String userName;
	private int  DrivePoint;
	private int  AnalyzePoint;
	private int  CreatePoint;
	private int  VolunteerPoint;
	private Date createdDate;
	private Date updatedDate;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getId() {
		return id;
	}

	public int getUserId() {
		return userId;
	}

	public int getDrivePoint() {
		return DrivePoint;
	}

	public int getAnalyzePoint() {
		return AnalyzePoint;
	}

	public int getCreatePoint() {
		return CreatePoint;
	}

	public int getVolunteerPoint() {
		return VolunteerPoint;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void setDrivePoint(int drivePoint) {
		DrivePoint = drivePoint;
	}

	public void setAnalyzePoint(int analyzePoint) {
		AnalyzePoint = analyzePoint;
	}

	public void setCreatePoint(int createPoint) {
		CreatePoint = createPoint;
	}

	public void setVolunteerPoint(int volunteerPoint) {
		VolunteerPoint = volunteerPoint;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
