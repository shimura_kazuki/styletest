package styletest.dto;

import java.sql.Date;

public class StyleResultDto {
	private int id;
	private int userId;
	private int drivePoint;
	private int analyzePoint;
	private int volunteerPoint;
	private int createPoint;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getDrivePoint() {
		return drivePoint;
	}
	public void setDrivePoint(int drivePoint) {
		this.drivePoint = drivePoint;
	}
	public int getAnalyzePoint() {
		return analyzePoint;
	}
	public void setAnalyzePoint(int analyzePoint) {
		this.analyzePoint = analyzePoint;
	}
	public int getVolunteerPoint() {
		return volunteerPoint;
	}
	public void setVolunteerPoint(int volunteerPoint) {
		this.volunteerPoint = volunteerPoint;
	}
	public int getCreatePoint() {
		return createPoint;
	}
	public void setCreatePoint(int createPoint) {
		this.createPoint = createPoint;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
