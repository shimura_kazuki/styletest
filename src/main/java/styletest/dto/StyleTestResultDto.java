package styletest.dto;

import java.sql.Date;

public class StyleTestResultDto {

	private int id;
	private int userId;
	private String userName;
	private int  drivePoint;
	private int  analyzePoint;
	private int  createPoint;
	private int  volunteerPoint;
	private Date createdDate;
	private Date updatedDate;

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getDrivePoint() {
		return drivePoint;
	}
	public void setDrivePoint(int drivePoint) {
		this.drivePoint = drivePoint;
	}
	public int getAnalyzePoint() {
		return analyzePoint;
	}
	public void setAnalyzePoint(int analyzePoint) {
		this.analyzePoint = analyzePoint;
	}
	public int getCreatePoint() {
		return createPoint;
	}
	public void setCreatePoint(int createPoint) {
		this.createPoint = createPoint;
	}
	public int getVolunteerPoint() {
		return volunteerPoint;
	}
	public void setVolunteerPoint(int volunteerPoint) {
		this.volunteerPoint = volunteerPoint;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}