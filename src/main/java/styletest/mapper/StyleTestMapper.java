package styletest.mapper;

import java.util.ArrayList;
import java.util.List;

import styletest.entity.OtherTestResult;
import styletest.entity.StyleTest;
import styletest.entity.StyleTestResult;

public interface StyleTestMapper {

//	問題文取得
	ArrayList<StyleTest> getTest();

//	スタイルテスト結果格納
	void resister(StyleTestResult entity);

	//スタイルテスト結果取得(1人)
	StyleTestResult  getStyleTestResult(int id);

//	過去の実績参照
	ArrayList<StyleTestResult> getOtherStyleTestResult(int id);

	//スタイルテスト全件取得
	List<StyleTestResult> getTestResultList();

//	testIdを用いて結果取得
	OtherTestResult getOtherTestResult(int id);

//テストフラグ回収
	void setTestResultFlag(int userId);

//	テスト受験済みか
	List<StyleTestResult> getTryCount(int userId);

	ArrayList<StyleTestResult> userStyleTestCheck(int id);
}


