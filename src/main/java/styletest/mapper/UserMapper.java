package styletest.mapper;

import java.util.ArrayList;

import styletest.dto.UserDto;
import styletest.entity.User;

public interface UserMapper {

	//ユーザー取得（1人）
	User getUser(int id);

	//ユーザー確認
	User getUser2(int EmployeeId);

	 //ユーザー新規登録
    int insertUser(UserDto dto);

	//ユーザー一覧取得
	ArrayList<User> getUserList();

	//ユーザー一覧取得（検索あり）
	ArrayList<User> searchUserList(int unitId);

    //更新する
  	int updateUser(UserDto dto);

    ArrayList<User> login(User entity);

	int isStop(UserDto dto);

	int isAnswer(UserDto dto);

	int isAnswerAll(int IsAnswered);

	ArrayList<User> userCheck(int id);

}
