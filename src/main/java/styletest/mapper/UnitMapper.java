package styletest.mapper;

import java.util.List;

import styletest.entity.Unit;

public interface UnitMapper {

	//全Unitを取得する
	List<Unit> getUnits();

}
