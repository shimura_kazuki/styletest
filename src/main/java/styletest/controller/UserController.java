package styletest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import styletest.dto.StyleTestResultDto;
import styletest.dto.UnitDto;
import styletest.dto.UserDto;
import styletest.form.UserForm;
import styletest.service.StyleTestService;
import styletest.service.UnitService;
import styletest.service.UserService;
import utils.CipherUtil;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private UnitService unitService;
	@Autowired
	private StyleTestService styleTestService;
	@Autowired
	HttpSession session;


		//データの登録 (GETリクエスト)
		@RequestMapping(value = "/signup/", method = RequestMethod.GET)
		public String signup(Model model) {
		    UserForm form = new UserForm();
		    model.addAttribute("userForm", form);
		    List<UnitDto> units = unitService.getUnits();
		    model.addAttribute("units", units);
		    return "signup";
		}


		//データの登録 (POSTリクエスト)
		@RequestMapping(value = "/signup/", method = RequestMethod.POST)
		public String signup(@ModelAttribute UserForm form, Model model) {
		    UserDto dto = new UserDto();
	    	List<String> messages = new ArrayList<String>();
	    	boolean isSignUp = true;

	    	if ( isValid(form, isSignUp, messages) ) {
	    		//パスワード暗号化
	    		if ( !form.getPassword().isEmpty() ) {
	    			String encPassword = CipherUtil.encrypt(form.getPassword());
	    			form.setPassword(encPassword);
	    		}
			    	BeanUtils.copyProperties(form, dto);
					int count = userService.insertUser(dto);
					List<UnitDto> units = unitService.getUnits();
				    ArrayList<UserDto> userList = new ArrayList<UserDto>();
					userList = userService.getUserList();
				    model.addAttribute("userForm", form);
					model.addAttribute("units", units);
				    model.addAttribute("userList", userList);
				    return "userlist";
	    	} else {
	    		List<UnitDto> units = unitService.getUnits();
		    	model.addAttribute("errorMessages", messages);
		    	model.addAttribute("userForm", form);
		    	model.addAttribute("units", units);
		    	return "signup";
	    	}
		}


		//データ一覧取得 (GETリクエスト)
		@RequestMapping(value = "/user/list/", method = RequestMethod.GET)
		public String userList(Model model) {
		    UserForm form = new UserForm();
		    UserDto loginUser = (UserDto) session.getAttribute("loginUser");
		    ArrayList<UserDto> userList = new ArrayList<UserDto>();
			userList = userService.getUserList();
		    List<UnitDto> units = unitService.getUnits();
		    model.addAttribute("userForm", form);
		    model.addAttribute("userList", userList);
		    model.addAttribute("units", units);
			model.addAttribute("loginUser",loginUser);
		    return "userlist";
		}

		//データ一覧取得検索 (GETリクエスト)
		@RequestMapping(value = "/user/list/{unitId}", method = RequestMethod.GET)
		public String userList(Model model, @PathVariable int unitId) {
			UserForm form = new UserForm();
		    UserDto loginUser = (UserDto) session.getAttribute("loginUser");
			ArrayList<UserDto> userList = new ArrayList<UserDto>();
			userList = userService.searchUserList(unitId);
			List<UnitDto> units = unitService.getUnits();
			model.addAttribute("userForm", form);
			model.addAttribute("userList", userList);
			model.addAttribute("units", units);
			model.addAttribute("loginUser",loginUser);
			return "userlist";
		}

		//データ一覧取得 (POSTリクエスト)
		@RequestMapping(value = "/user/list/", method = RequestMethod.POST)
		public String userList(@ModelAttribute UserForm form, Model model) {
		    UserDto dto = new UserDto();
			BeanUtils.copyProperties(form, dto);
		    return "userlist";
		}


		//データの更新 (GETリクエスト)
		@RequestMapping(value = "/user/update/{id}", method = RequestMethod.GET)
		public String userUpdate(Model model, @PathVariable int id) {
		    ArrayList<UserDto> ExistenceUsers = userService.userCheck(id);

		    if(ExistenceUsers.size() == 0) {
		    	System.out.println("error");
				model.addAttribute("message", "不正な値が入力されました");
				UserDto loginUser = (UserDto) session.getAttribute("loginUser");
				int tryCount = 0;
				List<StyleTestResultDto> tryCountList = styleTestService.getTryCount(loginUser.getId());
				for(int i = 0 ; i <tryCountList.size(); i++) {
					if(tryCountList.get(i).getUserId() == loginUser.getId()) {
						tryCount = tryCount+1;
					}
				}

				model.addAttribute("tryCount",tryCount);

				if(tryCount > 0) {

					StyleTestResultDto resultDto = new StyleTestResultDto();
					session.setAttribute("latestTestId", resultDto.getId());

					resultDto = styleTestService.getTestResult(loginUser.getId());
						if(resultDto != null) {
							int drivePoint =  resultDto.getDrivePoint();
							int analyzePoint =  resultDto.getAnalyzePoint();
							int createPoint =  resultDto.getCreatePoint();
							int volunteerPoint =  resultDto.getVolunteerPoint();
							int pointSum = drivePoint + analyzePoint + createPoint + volunteerPoint;
							model.addAttribute("drivePoint", 100 * drivePoint / pointSum);
							model.addAttribute("analyzePoint", 100 * analyzePoint / pointSum);
							model.addAttribute("createPoint", 100 * createPoint / pointSum);
							model.addAttribute("volunteerPoint", 100 * volunteerPoint / pointSum);
							model.addAttribute("testDate",resultDto.getCreatedDate());
						}

						ArrayList<StyleTestResultDto> otherResults = new ArrayList<StyleTestResultDto>();
						otherResults = styleTestService.getOtherResult(loginUser.getId());
						model.addAttribute("loginUser",loginUser);
						model.addAttribute("userid",loginUser.getId());
						model.addAttribute("name",loginUser.getName());
						model.addAttribute("otherResults",otherResults);

						return "index";

					} else {
						model.addAttribute("loginUser",loginUser);
						model.addAttribute("userid",loginUser.getId());
						model.addAttribute("name",loginUser.getName());

						return "index";
					}
		    } else {

		    	UserDto editUser = userService.getUser(id);
		    	model.addAttribute("editUser", editUser);
		    	UserForm form = new UserForm();
		    	UserDto loginUser = (UserDto) session.getAttribute("loginUser");
		    	form.setId(editUser.getId());
		    	form.setEmployeeId(editUser.getEmployeeId());
		    	form.setName(editUser.getName());
		    	form.setUnitId(editUser.getUnitId());
		    	form.setAdminFlag(editUser.getAdminFlag());
		    	model.addAttribute("userForm", form);
		    	List<UnitDto> units = unitService.getUnits();
		    	model.addAttribute("units", units);
		    	model.addAttribute("loginUser",loginUser);
		    	return "userUpdate";
		    }
		}


		// ユーザー更新 (POSTリクエスト)
		@RequestMapping(value = "/user/update/{id}", method = RequestMethod.POST)
		public String userUpdate(@ModelAttribute UserForm form, Model model) {
			UserDto dto = new UserDto();
			List<String> messages = new ArrayList<String>();
			boolean isSignUp = false;
			if ( isValid(form, isSignUp, messages) ) {
				//パスワード暗号化
				if ( !form.getPassword().isEmpty() ) {
					String encPassword = CipherUtil.encrypt(form.getPassword());
					form.setPassword(encPassword);
				} else {
					form.setPassword(null);
				}
				BeanUtils.copyProperties(form, dto);
				int count = userService.updateUser(dto);
				List<UnitDto> units = unitService.getUnits();
				ArrayList<UserDto> userList = new ArrayList<UserDto>();
				userList = userService.getUserList();
				model.addAttribute("userForm", form);
				model.addAttribute("units", units);
				model.addAttribute("userList", userList);
				return "userlist";
			}else {
				List<UnitDto> units = unitService.getUnits();
				model.addAttribute("errorMessages", messages);
				model.addAttribute("userForm", form);
				model.addAttribute("units", units);
				return "userUpdate";
			}
		}

		//ログイン (GETリクエスト)
		@RequestMapping(value = "/login/", method = RequestMethod.GET)
		public String login(Model model) {
			UserForm form = new UserForm();
			model.addAttribute("userForm", form);
			return "login";
		}

		//ログイン (Postリクエスト)
		@RequestMapping(value = "/login/", method = RequestMethod.POST)
		public String login(HttpServletRequest request,@ModelAttribute UserForm form,Model model) {

			UserDto dto = new UserDto();
			List<String> messages = new ArrayList<String>();
			if ( isValidLogin(form, messages) ) {
					//パスワード暗号化
		    	if ( !form.getPassword().isEmpty() ) {
		            String encPassword = CipherUtil.encrypt(form.getPassword());
		            form.setPassword(encPassword);
		        } else {
		        	form.setPassword(null);
		        }

		    	BeanUtils.copyProperties(form, dto);
				UserDto loginUser = new UserDto();
				loginUser = userService.login(dto);

//-------------------------------------------------------------------
				if ( StringUtils.isEmpty(loginUser) ) {
					messages.add("社員番号またはパスワードが違います");
					model.addAttribute("errorMessages", messages);
					return "login";
				} else {
					if(loginUser.getIsStopped() == 1) {
						messages.add("権限がありません");
						model.addAttribute("errorMessages", messages);
						return "login";

					} else {
						session.setAttribute("loginUser", loginUser);
						return "redirect:/index";
					}
				}
			} else {
				model.addAttribute("errorMessages", messages);
				return "login";
			}

//---------------------------------------------------------------
		}

		//ログアウト(GETリクエスト)
		@RequestMapping(value = "/logout/", method = RequestMethod.GET)
		public String logout(Model model) {
			session.invalidate();
			return "redirect:/login";
		}

		//停止
		@RequestMapping(value = "/user/isstop/{id}", method = RequestMethod.GET)
		public String isStop(Model model, @PathVariable int id) {
			UserDto dto = new UserDto();
		    ArrayList<UserDto> ExistenceUsers = userService.userCheck(id);

		    if(ExistenceUsers.size() == 0) {
		    	System.out.println("error");
				model.addAttribute("message", "不正な値が入力されました");
		    	return "redirect:/user/list/";
		    }else {
		    	dto.setId(id);
		    	dto.setIsStopped(1);
		    	userService.isStop(dto);
		    	return "redirect:/user/list/";
		    }
		}

		//復活
		@RequestMapping(value = "/user/isstart/{id}", method = RequestMethod.GET)
		public String isStart(Model model, @PathVariable int id) {
			UserDto dto = new UserDto();
		    ArrayList<UserDto> ExistenceUsers = userService.userCheck(id);

		    if(ExistenceUsers.size() == 0) {
		    	System.out.println("error");
				model.addAttribute("message", "不正な値が入力されました");
		    	return "redirect:/user/list/";

		    } else {
		    	dto.setId(id);
		    	dto.setIsStopped(0);
		    	userService.isStop(dto);
		    	return "redirect:/user/list/";
		    }
		}

		//解答権操作
		@RequestMapping(value = "/user/isanswered/{id}/", method = RequestMethod.GET)
		public String isAnswered(Model model, @PathVariable int id) {
			UserDto dto = new UserDto();
		    ArrayList<UserDto> ExistenceUsers = userService.userCheck(id);

		    if(ExistenceUsers.size() == 0) {
		    	System.out.println("error");
				model.addAttribute("message", "不正な値が入力されました");
		    	return "redirect:/user/list/";

		    } else {
		    	dto.setId(id);
		    	dto.setIsAnswered(0);
		    	userService.isAnswer(dto);
		    	return "redirect:/user/list/";

		    }
		}

		//	解答権全体操作
		@RequestMapping(value = "/user/answered/", method = RequestMethod.GET)
		public String isAnsweredAll(Model model) {
			UserDto dto = new UserDto();
			int IsAnswered = 0;
			userService.isAnswerAll(IsAnswered);
		    UserDto loginUser = (UserDto) session.getAttribute("loginUser");
		    loginUser.setIsAnswered(0);

			return "redirect:/user/list/";
		}

		private boolean isValid(UserForm form, boolean isSignUp,  List<String> messages) {
			String employeeId = String.valueOf(form.getEmployeeId());
			if ( StringUtils.isEmpty(form.getEmployeeId()) ) {
				messages.add("社員番号を入力してください");
			}
			if(!StringUtils.isEmpty(form.getEmployeeId()) && employeeId.matches("^[0-9]+$")){
				//バリデーション参考コード
				int EmployeeId = form.getEmployeeId();
				UserDto existUser = userService.getUser2(EmployeeId);
				if( isSignUp ) {
					if ( existUser != null ) {
						messages.add("社員番号が重複しています");
					}
				}else {
					if ( existUser != null && !form.getEmployeeId().equals(existUser.getEmployeeId())) {
						messages.add("社員番号が重複しています");
					}
				}
			}
			/*else {
				messages.add("社員番号は数字のみ使用可能です");
			}*/
			if ( StringUtils.isEmpty(form.getName()) ) {
				messages.add("名前を入力してください");
			}else {
				if(!form.getName().matches("\\S+$")) {
					messages.add("名前に空白が含まれています");
				}else {
					if(form.getName().length() > 10) {
						messages.add("名前は10文字以下で入力してください");
					}
				}
			}
			if ( StringUtils.isEmpty(form.getPassword()) ) {
				messages.add("パスワードを入力してください");
			}else {
				if(!form.getName().matches("\\S+$")) {
					messages.add("パスワードに空白が含まれています");
				}else {
					if(form.getPassword().length() < 6) {
						messages.add("パスワードは6文字以上入力してください");
					}else if(form.getPassword().length() > 20) {
						messages.add("パスワードは20文字以下で入力してください");
					}
				}
			}
			if ( StringUtils.isEmpty(form.getConfirmPassword()) ) {
				messages.add("確認用パスワードを入力してください");
			}else {
				if(!form.getName().matches("\\S+$")) {
					messages.add("確認用パスワードに空白が含まれています");
				}else {
					if ( !form.getPassword().equals(form.getConfirmPassword()) ) {
						messages.add("パスワードと確認用パスワードが一致しません");
					}
				}
			}
			if ( StringUtils.isEmpty(form.getUnitId()) ) {
				messages.add("所属Unitを入力してください");
			}
			if ( messages.size() == 0 ) {
				return true;
			} else {
				return false;
			}
		}

	//ログイン用のバリデーション
	private boolean isValidLogin(UserForm form, List<String> messages) {
    	if ( StringUtils.isEmpty(form.getEmployeeId()) ) {
    		messages.add("社員番号を入力してください");
    	}
    	if ( StringUtils.isEmpty(form.getPassword()) ) {
    		messages.add("パスワードを入力してください");
    	}


    	if ( messages.size() == 0 ) {
    		return true;
    	} else {
    		return false;
    	}
	}
}
