package styletest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import styletest.dto.OtherTestResultDto;
import styletest.dto.StyleTestDto;
import styletest.dto.StyleTestResultDto;
import styletest.dto.UserDto;
import styletest.form.StyleTestResultForm;
import styletest.service.StyleTestService;
import styletest.service.UserService;

@Controller
public class StyleTestController {

	@Autowired
	private StyleTestService styleTestService;
	@Autowired
	private UserService userService;
	@Autowired
	HttpSession session;


	//問題文取得 (GETリクエスト)
	@RequestMapping(value = "styleTest", method = RequestMethod.GET)
	public String styleTest(Model model) {

		ArrayList<StyleTestDto> styleTestList = new ArrayList<StyleTestDto>();
		styleTestList = styleTestService.getStyleTest();
	    UserDto loginUser = (UserDto) session.getAttribute("loginUser");

	    if(session.getAttribute("latestTestId") == null) {
	    	model.addAttribute("TestId",session.getAttribute("latestTestId"));
	    	System.out.println("無いよ");

	    }else {
	    	int testId = (int) session.getAttribute("latestTestId");
	    	model.addAttribute("TestId",testId);
	    	System.out.println("あるよ");

	    }

		model.addAttribute("styleTests",styleTestList);
		model.addAttribute("id",loginUser.getId());
		model.addAttribute("name",loginUser.getName());

		return "styleTest";
	}


//	診断結果送信(POSTリクエスト)
	@RequestMapping(value = "/styleTest/", method = RequestMethod.POST)
	public String styleTest(@ModelAttribute StyleTestResultForm form, Model model) {

		StyleTestResultDto dto = new StyleTestResultDto();
		BeanUtils.copyProperties(form, dto);
		styleTestService.ResultResister(dto);
		styleTestService.setTestResultFlag(dto.getUserId());

	    UserDto loginUser = (UserDto) session.getAttribute("loginUser");
	    loginUser.setIsAnswered(1);

	    int testId = (int) session.getAttribute("latestTestId");
	    System.out.println("testId"+testId);

		OtherTestResultDto otherTestResultDto = styleTestService.getOtherTestResult(testId);

		int drivePoint =  otherTestResultDto.getDrivePoint();
		int analyzePoint =  otherTestResultDto.getAnalyzePoint();
		int createPoint =  otherTestResultDto.getCreatePoint();
		int volunteerPoint =  otherTestResultDto.getVolunteerPoint();
		int pointSum = drivePoint + analyzePoint + createPoint + volunteerPoint;

		model.addAttribute("drivePoint", 100 * drivePoint / pointSum);
		model.addAttribute("analyzePoint", 100 * analyzePoint / pointSum);
		model.addAttribute("createPoint", 100 * createPoint / pointSum);
		model.addAttribute("volunteerPoint", 100 * volunteerPoint / pointSum);
		model.addAttribute("testDate",otherTestResultDto.getCreatedDate());

		ArrayList<StyleTestResultDto> otherResults = new ArrayList<StyleTestResultDto>();
		otherResults = styleTestService.getOtherResult(otherTestResultDto.getUserId());
		model.addAttribute("userid",otherTestResultDto.getUserId());
		model.addAttribute("name",otherTestResultDto.getUserName());
		model.addAttribute("otherResults",otherResults);
		return "index";
	}

		//サマリー（まとめ）表示
		@RequestMapping(value = "/summary/", method = RequestMethod.GET)
		public String summary(Model model) {
			List<StyleTestResultDto> testResultList = styleTestService.getTestResultList();
			int drivePoint =  0;
			int analyzePoint =  0;
			int createPoint =  0;
			int volunteerPoint =  0;
			for ( int i = 0; i < testResultList.size(); i++ ) {
				drivePoint += testResultList.get(i).getDrivePoint();
				analyzePoint += testResultList.get(i).getAnalyzePoint();
				createPoint += testResultList.get(i).getCreatePoint();
				volunteerPoint += testResultList.get(i).getVolunteerPoint();
			}
			double pointSum = drivePoint + analyzePoint + createPoint + volunteerPoint;
			model.addAttribute("drivePoint", 100 * drivePoint / pointSum);
			model.addAttribute("analyzePoint", 100 * analyzePoint / pointSum);
			model.addAttribute("createPoint", 100 * createPoint / pointSum);
			model.addAttribute("volunteerPoint", 100 * volunteerPoint / pointSum);
		    return "summary";
		}

		//個人の結果取得 (GETリクエスト)
		@RequestMapping(value = "{id}", method = RequestMethod.GET)
		public String otherTestResult(Model model,@PathVariable int id) {
//		    ArrayList<UserDto> ExistenceUsers = userService.userCheck(id);
		    ArrayList<StyleTestResultDto> ExistenceUsers = styleTestService.userStyleTestCheck(id);

		    if(ExistenceUsers.size() == 0) {
		    	System.out.println("error");
				model.addAttribute("message", "不正な値が入力されました");
		    	return "index";

		    } else {
			OtherTestResultDto otherTestResultDto = styleTestService.getOtherTestResult(id);

			int drivePoint =  otherTestResultDto.getDrivePoint();
			int analyzePoint =  otherTestResultDto.getAnalyzePoint();
			int createPoint =  otherTestResultDto.getCreatePoint();
			int volunteerPoint =  otherTestResultDto.getVolunteerPoint();
			int pointSum = drivePoint + analyzePoint + createPoint + volunteerPoint;

			model.addAttribute("drivePoint", 100 * drivePoint / pointSum);
			model.addAttribute("analyzePoint", 100 * analyzePoint / pointSum);
			model.addAttribute("createPoint", 100 * createPoint / pointSum);
			model.addAttribute("volunteerPoint", 100 * volunteerPoint / pointSum);
			model.addAttribute("testDate",otherTestResultDto.getCreatedDate());

			ArrayList<StyleTestResultDto> otherResults = new ArrayList<StyleTestResultDto>();
			otherResults = styleTestService.getOtherResult(otherTestResultDto.getUserId());
			model.addAttribute("userid",otherTestResultDto.getUserId());
			model.addAttribute("name",otherTestResultDto.getUserName());
			model.addAttribute("otherResults",otherResults);
			return "index";
		    }
		}

		//過去の結果取得 (GETリクエスト)-----------------------------------------------
		@RequestMapping(value = "/index/{id}", method = RequestMethod.GET)
		public String otherUserTestResult(Model model,@PathVariable int id) {
//		    ArrayList<UserDto> ExistenceUsers = userService.userCheck(id);
		    ArrayList<StyleTestResultDto> ExistenceUsers = styleTestService.userStyleTestCheck(id);

		    if(ExistenceUsers.size() == 0) {
		    	System.out.println("error");
				model.addAttribute("message", "不正な値が入力されました");
		    	return "index";

		    } else {

			StyleTestResultDto resultDto = new StyleTestResultDto();
			int tryCount = 0;

			List<StyleTestResultDto> tryCountList = styleTestService.getTryCount(id);

			for(int i = 0 ; i <tryCountList.size(); i++) {

				if(tryCountList.get(i).getUserId() == id) {
					tryCount = tryCount+1;
				}
			}

			model.addAttribute("tryCount",tryCount);

			if(tryCount > 0) {

				resultDto = styleTestService.getTestResult(id);

				int drivePoint =  resultDto.getDrivePoint();
				int analyzePoint =  resultDto.getAnalyzePoint();
				int createPoint =  resultDto.getCreatePoint();
				int volunteerPoint =  resultDto.getVolunteerPoint();
				int pointSum = drivePoint + analyzePoint + createPoint + volunteerPoint;
				model.addAttribute("drivePoint", 100 * drivePoint / pointSum);
				model.addAttribute("analyzePoint", 100 * analyzePoint / pointSum);
				model.addAttribute("createPoint", 100 * createPoint / pointSum);
				model.addAttribute("volunteerPoint", 100 * volunteerPoint / pointSum);
				model.addAttribute("testDate",resultDto.getCreatedDate());
				ArrayList<StyleTestResultDto> otherResults = new ArrayList<StyleTestResultDto>();
				otherResults = styleTestService.getOtherResult(id);
				model.addAttribute("userid",resultDto.getId());
				model.addAttribute("name",resultDto.getUserName());
				model.addAttribute("otherResults",otherResults);
				return "index";

			   } else {
					UserDto UserDto = userService.getUser(id);
					model.addAttribute("userid",UserDto.getId());
					model.addAttribute("name",UserDto.getName());
					return "index";
			   }
			}
		}



		//		indexへ飛ばす
		@RequestMapping(value = "/index", method = RequestMethod.GET)
		public String index(Model model) {

			UserDto loginUser = (UserDto) session.getAttribute("loginUser");

			int tryCount = 0;

			List<StyleTestResultDto> tryCountList = styleTestService.getTryCount(loginUser.getId());

			for(int i = 0 ; i <tryCountList.size(); i++) {

				if(tryCountList.get(i).getUserId() == loginUser.getId()) {
					tryCount = tryCount+1;
				}
			}

			model.addAttribute("tryCount",tryCount);

			if(tryCount > 0) {

			StyleTestResultDto resultDto = new StyleTestResultDto();

			resultDto = styleTestService.getTestResult(loginUser.getId());
			session.setAttribute("latestTestId", resultDto.getId());

				if(resultDto != null) {
					int drivePoint =  resultDto.getDrivePoint();
					int analyzePoint =  resultDto.getAnalyzePoint();
					int createPoint =  resultDto.getCreatePoint();
					int volunteerPoint =  resultDto.getVolunteerPoint();
					int pointSum = drivePoint + analyzePoint + createPoint + volunteerPoint;
					model.addAttribute("drivePoint", 100 * drivePoint / pointSum);
					model.addAttribute("analyzePoint", 100 * analyzePoint / pointSum);
					model.addAttribute("createPoint", 100 * createPoint / pointSum);
					model.addAttribute("volunteerPoint", 100 * volunteerPoint / pointSum);
					model.addAttribute("testDate",resultDto.getCreatedDate());
				}

				ArrayList<StyleTestResultDto> otherResults = new ArrayList<StyleTestResultDto>();
				otherResults = styleTestService.getOtherResult(loginUser.getId());
				model.addAttribute("loginUser",loginUser);
				model.addAttribute("userid",loginUser.getId());
				model.addAttribute("name",loginUser.getName());
				model.addAttribute("otherResults",otherResults);

				return "index";

			} else {
				model.addAttribute("loginUser",loginUser);
				model.addAttribute("userid",loginUser.getId());
				model.addAttribute("name",loginUser.getName());

				return "index";
			}
		}

}