package styletest.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import styletest.dto.UserDto;

@WebFilter("/*")
public class SystemAdminFilter implements Filter{


	@Autowired
	HttpSession session;

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain)throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		String update = "/update/";
		String signup = "/signup/";
		String summary = "/summary/";

		String ExcludeUrl = ((HttpServletRequest) request).getServletPath();




		if ( ExcludeUrl.contains(update) || ExcludeUrl.contains(signup) || ExcludeUrl.contains(summary)) {

			HttpSession session = ((HttpServletRequest)request).getSession();
			UserDto user = (UserDto) session.getAttribute("loginUser");
			int adminFrag = user.getAdminFlag();

			if(adminFrag == 1) {
				chain.doFilter(request, response);

			}else {
				messages.add("管理者権限がありません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("/StyleTest/index/");

			}
		} else {
			chain.doFilter(request, response);
		}
	}



	@Override
	public void destroy() {

	}


	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
