package styletest.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import styletest.dto.UserDto;

@WebFilter("/*")
public class LoginFilter implements Filter{

	@Autowired
	HttpSession session;

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain)throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		String reqUrl = "/login/";

		String ExcludeUrl = ((HttpServletRequest) request).getServletPath();

		HttpSession session = ((HttpServletRequest)request).getSession();
		UserDto user = (UserDto) session.getAttribute("loginUser");


		if ( ExcludeUrl.equals(reqUrl) || ExcludeUrl.contains("/css") ) {
			chain.doFilter(request, response);
		} else if ( user == null ) {
			messages.add("ログインしてください");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("/StyleTest/login/");
		} else {
			chain.doFilter(request, response);
		}
	}



	@Override
	public void destroy() {

	}


	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
